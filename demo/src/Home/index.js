import React, { Component } from 'react'

import RouterView from '../Router/RouterView';
import {NavLink} from "react-router-dom"

import { Button} from 'react-vant';

import { Search ,ClosedEye} from '@react-vant/icons';

import IndexModule from "./Index.module.scss"

export default class index extends Component {
  render() {
      let {RouterList,navLink}=this.props
    return (
      <div className={IndexModule.Home}>
          <div className={IndexModule.header}>
              <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" height="36px" alt='' className={IndexModule.img}/>
              <div className={IndexModule.nav}>
                {
                    navLink&&navLink.length?navLink.map((item,index)=>{
                      return <NavLink to={item.path} key={index} className={IndexModule.active?"red":""}>{item.name}</NavLink>
                    }):<div>暂无数据</div>
                }
              </div>
              <ul className={IndexModule.header_right}>
                <li>切换</li>
                <li><ClosedEye/></li>
                <li><Search/></li>
                <li><Button type="primary">登录</Button></li>
              </ul>
          </div>
          <div className={IndexModule.main}>
                <RouterView RouterList={RouterList}/>
          </div>
      </div>
    )
  }
}
