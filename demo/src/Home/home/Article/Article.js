import React, { Component } from 'react';

import { Swiper } from 'react-vant';

import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import * as actions from "../../../action/index"

import articleModule from "./article.module.scss"
 
class Article extends Component {

    componentDidMount(){
        this.props.get_article_list()
    }

    render() { 
        let {articleList}=this.props
        console.log(articleList);
        return (
            <div className={articleModule.article}>
                <div className={articleModule.article_left}>
                    <Swiper className={articleModule.my_swipe} autoplay={3000}>
                        <Swiper.Item>1</Swiper.Item>
                        <Swiper.Item>2</Swiper.Item>
                        <Swiper.Item>3</Swiper.Item>
                        <Swiper.Item>4</Swiper.Item>
                    </Swiper>
                </div>
                <div className='article_right'>
                    
                </div>
            </div>
        );
    }
}

let mapStateToProps=({ArticleReducer})=>{
    return{
        ...ArticleReducer
    }
}

let mapDispatchToProps=(dispatch)=>bindActionCreators(actions,dispatch)
 
export default connect(mapStateToProps,mapDispatchToProps)(Article);