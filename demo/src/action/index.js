import {getArticle} from "../api/index"

// 获取文章信息
export let get_article_list=()=>{
    return async (dispatch)=>{
        let {data}=await getArticle()
        dispatch({
            type:"GETARTICLE_LIST",
            payload:data
        })
    }
}