
import {Suspense} from "react"
import {Route,Redirect,Switch} from "react-router-dom"
 
const RouterView = (props) => {
    let {RouterList}=props
    let routerList=RouterList.filter((item)=>!item.to)
    let redirect=RouterList.filter((item)=>item.to)[0]
    return (
        <Suspense fallback={<div>lodding...</div>}>
            <Switch>
                {
                    routerList&&routerList.length?routerList.map((item,index)=>{
                        return <Route path={item.path} key={index} render={routerProps=>{
                            let Son=item.component;
                            if(item.children){
                                return <Son {...routerProps} RouterList={item.children} navLink={item.children.filter(val=>!val.to)}></Son>
                            }
                            return <Son {...routerProps}></Son>
                        }}></Route>
                    }):<div>暂无数据</div>
                }
                {
                    redirect&&<Redirect to={redirect.to} from={redirect.from}/>
                }
            </Switch>
        </Suspense>
    );
}
 
export default RouterView;