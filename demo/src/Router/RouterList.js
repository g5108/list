
import {lazy} from "react"

const RouterList=[{
    path:"/Home",
    component:lazy(()=>import("../Home/index")),
    children:[{
        path:"/Home/Article",
        component:lazy(()=>import("../Home/home/Article/Article")),
        name:"文章"
    },{
        path:"/Home/File",
        component:lazy(()=>import("../Home/home/File/File")),
        name:"归档"
    },{
        path:"/Home/Know",
        component:lazy(()=>import("../Home/home/Know/Know")),
        name:"知识小册"
    },{
        path:"/Home/Message",
        component:lazy(()=>import("../Home/home/Message/Message")),
        name:"留言板"
    },{
        path:"/Home/About",
        component:lazy(()=>import("../Home/home/About/About")),
        name:"关于"
    },{
        to:"/Home/Article",
        from:"/Home"
    }]
},{
    to:"/Home",
    from:"/"
}]

export default RouterList