import {createStore,combineReducers,applyMiddleware} from "redux"

import thunk from "redux-thunk"
import logger from "redux-logger"

import ArticleReducer from "./module/ArticleReducer"

let reducer=combineReducers({
    ArticleReducer
})

export default createStore(reducer,applyMiddleware(thunk,logger))