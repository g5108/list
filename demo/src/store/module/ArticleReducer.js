let initData={
    articleList:[]
}

let ArticleReducer=(state=initData,{type,payload})=>{
    switch (type) {
        case "GETARTICLE_LIST":
            return {
                ...state,
                articleList:payload
            }
        default:
            return{
                ...state
            }
    }
}

export default ArticleReducer