import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import RouterView from "./Router/RouterView"
import RouterList from "./Router/RouterList"

import {BrowserRouter as Router} from "react-router-dom"

import "./css/style.css"
import 'react-vant/lib/index.css';

import {Provider} from "react-redux"
import store from "./store/index"

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RouterView RouterList={RouterList}></RouterView>
    </Router>
  </Provider>,
  document.getElementById('root')
);